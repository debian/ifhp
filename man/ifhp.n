.TH IFHP 8 "_VERSION_"
.hy 0
.SH NAME
ifhp \- Almost Universal LPRng Print Filter
.\" $Id: ifhp.n,v 3.16 2002/04/01 18:37:42 papowell Exp papowell $
.SH SYNOPSIS
.B ifhp
.br
[-F format]
[-s statusfile]
[-?value (other LPRng filter options)]
.br
.br
[-Z[options[,options]*]*
.br
[-T[options[,options]*]*
.br
[accountingfile]
.br
.sp
.BR ifhp.conf "(configuration file)"
.SH DESCRIPTION
.LP
The
.B ifhp
is a general purpose filter for 
allmost all PostScript, PJL, PCL, and text based line printers.
.LP
The
.B ifhp 
filter
resets and synchronizes  the printer,
gets accounting information,
and then checks the file to determine its format.
Based on the format,
it will do PCL, PostScript, or other initializations,
and then transfer the file to the printer.
Termination sequences are sent,
followed by getting accounting information.
.SH "OPTIONS AND CONFIGURATION"
.LP
Options controlling
.B ifhp
operations are specified by
.BR -T option
or
.BR -Z option
values.
These options have the form:
.nf
Form          Same As
flag          flag=1
flag@         flag=0
flag=str  
flag#str
.fi
.LP
The complete set of options and their effect on operation are detailed
in the IFHP-HOWTO document.
The following is a brief list of the more important ones that will
have a dramatic effect on printer operation.
.sp .5v
.TP
.BR model	=modelid
.br
.fi
Specifies the printer modelid information.
This is used to select an appropriate configuration from the ifhp configuration file.
.TP
.B "status (status@)"
.br
.fi
The 
.B status
option enables checking for status information from the printer.
See
.B "STATUS, SYNC, WAITEND, AND PAGECOUNT"
for details.
.TP
.B "sync (sync@, sync=ps, sync=pjl)"
.TP
.B "waitend (waitend@, waitend=ps, waitend=pjl)"
.br
.fi
If the
.B status
option is enabled,
.B sync
will check to see that the printer is online and ready
and the
.B waitent
will check that the job has finished.
The
.B "sync@"
and
.B "waitend@"
will suppress these checks.
See
.B "STATUS, SYNC, WAITEND, AND PAGECOUNT"
for details.
.TP
.B "pagecount (pagecount@, pagecount=ps, pagecount=pjl)"
.br
.fi
If the
.B status
option is enabled,
.B pagecount
will try to get the value of the printer pagecounter.
See
.B "STATUS, SYNC, WAITEND, AND PAGECOUNT"
for details.
.TP
.BR config =path,path,...
.br
.fi
pathnames of configuration files to read to obtain printer configuration information.
.TP
.BR debug  =debuglevel
.br
.fi
Set the debugging level.
A larger number causes more verbose error messages.
You have been warned.
.TP
.B trace
.br
.fi
Sends out debugging and tracing information on STDERR as well as to the status
log file.
.TP
.BR stty =options
.br
.fi
Sets serial line configuration when a dev entry specified.
.SH "STATUS, SYNC, WAITEND, AND PAGECOUNT"
.LP
If the
.B status
option is true,
then the
.B ifhp
filter will assume that the printer is attached by a bidirectional channel
and will attempt to read status information from the printer.
This also enables the
.B sync,
.B sync
and
.B pagecount
handshaking and getting pagecount information from the printer.
.LP
The
.B sync
flag causes a special PJL job or PostScript job to be sent to the printer
at the start of printing
at periodic intervals.
This job has the property that it will attempt to put the printer into
a sane state and cause it to return status information.
By default,
if the printer supports PJL then the PJL job is used,
otherwise if it supports PostScript, the PostScript job is used.
You can force the sync method using
.B "sync=pjl"
or
.B "sync=ps"
if necessary,
and suppress it using
.BR "sync@"
.LP
The
.B waitend
flag causes a special PJL job or PostScript job to be sent to the printer
at the end of the job
at periodic intervals.
This job has the property that it will attempt to put the printer into
a sane state and cause it to return status information.
By default,
if the printer supports PJL then the PJL job is used,
otherwise if it supports PostScript, the PostScript job is used.
You can force the waitend method using
.B "waitend=pjl"
or
.B "waitend=ps"
if necessary,
and suppress it using
.BR "waitend@"
.LP
The
.B pagecount
flag causes a special PJL job or PostScript job to be sent to the printer
at the start and end of the job.
This job has the property that it causes the printer to return
the value of a hardware page counter.
By taking the difference of the start and end values of the pagecounter,
an accurate measure of the number of pages (or impressions for some duplex
printers) can be done.
By default,
if the printer supports PJL then the PJL job is used,
otherwise if it supports PostScript, the PostScript job is used.
You can force the pagecount method using
.B "pagecount=pjl"
or
.B "pagecount=ps"
if necessary,
and suppress it using
.BR "pagecount@"
.LP
The IFHP filter will puts tracing and status information in the file
specified by the
.B -s
command line flag.
If the status file does not exist,  it will not be created.
The diagnostic
.B trace
flag overrides this and directs the status to STDERR.
.LP
The pagecount information is put into the accounting file specified on the
command line.
If the accounting file does not exist,  it will not be created.
.SH "JOB PROCESSING (LPR -Z) OPTIONS"
.LP
The
.B lpr
program passes the command line -Z options
or the
.B lp
program -o options
to the LPRng
.B lpd
server
which then passes them to the ifhp filter.
The options are used to control various printing features.
For example if duplex printing and landscape is wanted, then
.sp .5v
.B 	lpr -P<printer> -Z'duplex,landscape' <file> 
.sp .5v
.LP
Since each printer has a different set of capabilities
there are no standard set of -Z options.
The following is a sample of the
options provided by
.BR ifhp .
Check the ifhp.conf file for all of the options supported
by the printer.
.TP
.B "Input Tray Selection"
.br
inupper, inlower, intray1, intray2, intray3, manual, envelope
.br
source=name
.br
The input tray selection options should start with the
.B in
prefix and corresponds to the various trays.
The manual and envelope options are included to select manual
feed or envelope feed. There is a possible source of conflict
here as there may be an envelope feeder as well as an envelope
media. This is a printer specific dependency.
.br
The source=name option allows users to use options such as
-Zsource=inbin1, which may be useful for systems that have
an unusual or nonstandard input selection mechanism.
se select media (paper) from the specified tray or feeder.
.TP
.B "Output Bin Selection"
.br
outupper, outlower, outbin1
.br
outbin=name
.br
The output bin selection should start with the out prefix.
.br
The outbin=name form allow users to use options such as
-Zoutbin=stapler, which may be useful for systems that have
an unusual or nonstandard output selection mechanism.
.TP
.B "Media Size Selection"
.br
.nf
a0 a1 a2 a3 a4 a5 a6 a7 a8 a9 a10 b0 b1 b2 b3 b4 b5
archA archB archC archD archE flsa flse halfletter
note letter legal 11x17 ledger tabloid
.fi
.br
paper=name
.br
The paper size selection facilities usually are quite printer
dependent, and the input tray selection and paper size selection
mechanisms may interact in strange and mysterious ways.
.br
The paper=name form allow users to use options such as
-Zpaper=b3, which may be useful for systems that have an
unusual or nonstandard input media selection mechanism.
.TP
.B "Media Type Selection"
.br
plain, preprinted, letterhead, transparency, glossy, prepunched, labels
.br
bond, recycled, color, heavy, cardstock
.br
Media Type is not the same as paper size, and corresponds to the
name assigned to a particular media. Of course, the issue is
complicated by the fact that some media have standard sizes as
well. Again, the input tray selection, media size, and media type
selection will interact in confusing and mysterious ways, depending
on the whim of the printer firmware iplementors.
.br
You will notice that there is no generic mediatype=name facility.
This is due to the radically different way that PostScript,
PJL, PCL, etc., each handle media name and selection.  While
it is possible to determine the various strings, numbers, escape
codes, etc., that need to be sent for each mediatype,  a general
mechanism is too fragile and fraught with peril for the unwary
administrator to provide.  You have been warned.  Been there and
have the scars to prove it.
.br
These are commonly used media type names gleaned from various
PostScript Printer Description Files, Microsoft printer
drivers, and arcane lore of the Printer Working Group. Note
that these are not accepted terms in the paper industry for
any of these type of media. You are warned.
.TP
.B "Duplex/Simplex"
.br
duplex (lduplex)
duplexshort (sduplex)
.br
simplex
.br
tumble (shortedge)
.br
The duplex (alias lduplex) and duplexshort (alias sduplex)
will print on both sides of the paper; duplexshort will
reverse the orientation; simplex will print only on one side
of the paper.
.br
The tumble (shortedge) will reverse the orientation of the paper.
Used when printing facing pages of duplex and there is a desperate
need for this orientation.
.TP
.B "Copies"
.br
copies=N
.br
Be aware that this option is totally bogus
is usually ignored (except when you expect it to be ignored).
You have been warned.
.SH "OF MODE"
The
.B OF
mode of operation is selected by the
.B -Fo
command line option.
When operationg in OF mode it will
scan the input for a two character stop sequence \e031\e011.
When it finds the sequence it will suspend itself.
The spooler will restart the filter with a SIGCONT signal
when it has more output for it. 
.SH FILES
.PD 0
.TP
.B "_IFHP_CONF_"
.br
Configuration files
.TP
.B "_FILTER_DIR_/ifhp"
.br
Executables.
.SH "SEE ALSO"
.LP
The IFHP-HOWTO document,
which is included as part of the IFHP distribution.
This is the principle reference for this filter.
.sp
.LP
.BR lpd (8),
.BR printcap (5).
.sp
.LP
.nf
FTP sites: ftp://ftp.lprng.com
.sp
.LP
WEB sites: http://www.lprng.com
.sp
.LP
Mailing List.  Send email to lprng-request@lprng.com
with the word 'subscribe' in the boyd.
.SH "BUGS"
.LP
They are not bugs.  They are mis-documented features.
.SH "AUTHOR"
.LP
The IFHP filter was inspired by the LPRng CTI-ifhp filter,
which was a descendent of the print filters developed by the
.B CTI-Print 
project at the 
.B Division of Computing Facilities 
of the 
.B Computer Technology Institute (CTI), Patras, Greece.
.SH "AUTHORS"
.nf
.in +.5i
.ti -.5i
Patrick Powell,
Astart Technologies,
San Diego, CA
<papowell@lprng.com>
