.TH TEXTPS 8 "_VERSION_"
.\" $Id: textps.n,v 1.1 1998/12/29 02:54:32 papowell Exp papowell $
.SH NAME
textps \- text to PostScript filter
.br
nscript \- text to PostScript converter
.SH SYNOPSIS
.B textps
[
.BI \-T [option[,option]*]*
]
[
.I "lpr filter options"
]
[
.I filename\|.\|.\|.
]
.sp
options:
[
.BI c =n
]
[
.BI l =n
]
[
.BI m =n
] [
.BI t =n
]
[
.BI v =n
]
.sp
.B nscript
[
.B -G
] [
.B -r
] [
.B -2
] [
.B -t width
] [
.BI -p file
] [
.BI -P printer
] [
.I filename\|.\|.\|.
]
.SH DESCRIPTION
.B textps
is a simple text to PostScript filter.
When invoked as
.B nscript
it (weakly) simulates the Adobe Systems Incorporated
.B enscript
program,
converting text to PostScript and adding headers.
.B textps
is usually invoked automatically by a print spooler while
.B nscript
is usually invoked by a user.
Input files can use ISO Latin-1.
Two or more identical overstruck characters are rendered using a bold font.
Tabs are assumed to be set every 8 character positions.
.B textps
is designed to produce good output with
.B lpr\ \-p
or
.BR pr\ |\ lpr ;
if you print unpaginated text,
you will need to increase the
.B t
option value and decrease the
.B v
or
.B l
option value.
.SH "TEXTPS OPTIONS"
.TP
.BI c =n
Use
.I n
characters to the inch.
.TP
.BI l =n
Use
.I n
lines per page.
.TP
.BI m =n
Use a left margin of
.I n
points.
.TP
.BI t =n
Position the baseline of the first line of text
.I n
points below the top of the page.
.TP
.BI v =n
Use a vertical spacing of
.I n
points.
.LP
The default behaviour is
.BR -Tc=12,l=66,m=18,t=8,v=12 .
.SH "NSCRIPT OPTIONS"
By default,
.B nscript
will send the formatted output to lpr;
the -P option can select the printer.
.TP
.B -G
Gaudy output - a header and labels for the input files are placed on the output
page.
.TP
.B -r
rotated (landscape) output.
.TP
.BI -t width
Set tab stop to
.I width
characters.
.TP
.B -2
two column output.
.TP
.BI -p file
send output to
.I file.
.TP
.BI -P printer
spool output to
.I printer
using
.BR lpr .
.LP
Note: the combination nscript -2Gr is useful for viewing listings.
.LP
.SH "AUTHORS"
.nf
Original Author:
   James Clark <jj@jclark.com>,  used with with his permission;
Modified by:
   Patrick Powell <papowell@sdsu.edu>
.SH "SEE ALSO"
.BR psrev (1),
.BR psfilter (1),
.BR psbanner (1)
