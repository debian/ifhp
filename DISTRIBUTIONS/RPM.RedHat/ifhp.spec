Summary: The IFHP filter for the LPRng print spooler.
Name: ifhp
Version: 3.5.20
Release: 1
Vendor: Patrick Powell, LPRng.com <papowell@astart.com>
License: GPL and Artistic
Group: System Environment/Daemons
Source: ftp://ftp.lprng.com/pub/LPRng/ifhp/%{name}-%{version}.tgz
URL: http://www.lprng.com
Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root
Provides: ifhp
Prereq: mktemp, fileutils, textutils, gawk, ghostscript
BuildPreReq: gettext

%description
The ifhp print filter is the main print filter for the LPRng
print spooler.  It has support for PCL, PostScript, PJL,
and text printers,  as well as a flexible configuration system
that allows it to be configured to handle a wide variety of
unusual or legacy printers.

%changelog
* Sat Sep 29 2001 Patrick Powell <papowell@astart.com>
  - added warning about GhostScript able to read files
  - added --disable-gscheck to configuration options 
* Mon Aug 27 2001 Patrick Powell <papowell@astart.com>
  - Updated the installation files
* Tue Nov 28 2000 Patrick Powell <papowell@astart.com>
  - Updated the installation and files generation
    Require PostScript, a2ps or mpage
* Thu Jun 15 2000 Patrick Powell <papowell@astart.com>
  - Updated the installation and files generation
* Sun May  7 2000 Patrick Powell <papowell@astart.com>
  - update the scripts.  Added a dynamic %files capability
    added preinstall, preremove, and postinstall scripts
* Sat Sep  4 1999 Patrick Powell <papowell@astart.com>
  - did ugly things to put the script in the spec file
* Sat Aug 28 1999 Giulio Orsero <giulioo@tiscalinet.it>
  - 3.6.8
* Fri Aug 27 1999 Giulio Orsero <giulioo@tiscalinet.it>
  - 3.6.7 First RPM build.
  
%prep

%setup -q

# pick up configure.in changes

# set up gettext
(
cd po
rm -f Makefile.in.in
ln -s /usr/share/gettext/po/Makefile.in.in .
)

# sh CREATE_CONFIGURE
    
%build
CFLAGS="$RPM_OPT_FLAGS" ; export CFLAGS
%configure --enable-nls --disable-gscheck

make

%install
rm -rf %{buildroot}

# Installation of locales is broken... Work around it!
#perl -pi -e "s,prefix =.*,prefix = ${RPM_BUILD_ROOT}%{_prefix},g" po/Makefile
#perl -pi -e "s,datadir =.*,datadir = ${RPM_BUILD_ROOT}%{_prefix}/share,g" po/Makefile
#perl -pi -e "s,localedir =.*,localedir = ${RPM_BUILD_ROOT}%{_prefix}/share/locale,g" po/Makefile
#perl -pi -e "s,gettextsrcdir =.*,gettextsrcdir = ${RPM_BUILD_ROOT}%{_prefix}/share/gettext/po,g" po/Makefile

make mandir=%{_mandir} DESTDIR=${RPM_BUILD_ROOT} install

%post

echo "(/etc/hosts) (r) file quit" | gs -q -dPARANOIDSAFER -dSAFER -sDEVICE=nullpage -sOutputFile=- -
status=$?
if [ "$status" = "0" ] ; then 
  echo "WARNING: security loophole in GhostScript will allow files to be read"
  echo "See the README.GhostScriptSecurityProblem document for details"
fi

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%attr(644,root,root) %{_sysconfdir}/ifhp.conf.sample
%config %{_sysconfdir}/ifhp.conf
%{_libexecdir}/filters/*
%{_bindir}/*
%{_mandir}/*/*
%attr(755,root,root) %{_prefix}/share/locale/*
%doc CHANGES Copyright INSTALL LICENSE
%doc README* VERSION
%doc DOCS/*.html DOCS/*.pdf DOCS/*.jpg
