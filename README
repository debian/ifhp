                      README for IFHP
                    ===================

                Patrick Powell <papowell@astart.com>
                Original Sun Oct 29 06:25:05 PST 1995
                 Revised Wed Dec 20 05:18:16 PST 2000

ifhp is a highly versatile print filter for BSD based print spoolers.
It can be configured to handle text,  PostScript, PJL, PCL, and
raster printers, supports conversion from one format to another,
and can be used as a stand-alone print utility.  It is the primary
supported print filter for the LPRng print spooler.

Web Page and HOWTO in HTML format:
   http://www.lprng.com/

The software may be obtained from
   ftp://ftp.lprng.com/pub/LPRng  (Main site)

Mirrors:
    ftp://ftp.cs.columbia.edu/pub/archives/pkg/LPRng/FILTERS  (US)
    ftp://ftp.cs.umn.edu/pub/LPRng/FILTERS  (US)
    ftp://uiarchive.uiuc.edu/pub/packages/LPRng/FILTERS  (US)
    ftp://ftp.sage-au.org.au/pub/printing/spooler/lprng//FILTERS  (AU) 
    ftp://mirror.aarnet.edu.au/pub/LPRng//FILTERS  (AU/NZ)
    http://mirror.aarnet.edu.au/pub/LPRng//FILTERS  (AU/NZ)
    ftp://sunsite.ualberta.ca/pub/Mirror/LPRng/FILTERS  (CA)
    ftp://ftp.informatik.uni-hamburg.de/pub/os/unix/utils/LPRng/FILTERS  (DE)
    ftp://ftp.uni-paderborn.de/pub/unix/printer/LPRng/FILTERS  (DE)
    ftp://ftp.mono.org/pub/LPRng/FILTERS  (UK)
    ftp://ftp.iona.com/pub/plp/LPRng/FILTERS  (IE)
    ftp://uabgate.uab.ericsson.se/pub/unix/LPRng/FILTERS  (SE)

0. The Express Install:
   
   configure     (use defaults)
     or
   configure --prefix=/usr --sysconfdir=/etc
     or
   sh STANDARD_configuration (you can use this script)

   make clean all
   AS ROOT:
     make install

1.  Problems, Questions, and Answers

    The best place to get answers is the LPRng mailing list.
    To subscribe, send mail to: lprng-request@iona.ie
    with the word
         subscribe 
    in the body.

    Patrick Powell                 Astart Technologies
    papowell@astart.com            6741 Convoy Court
    Network and System             San Diego, CA 92111
      Consulting                   858-874-6543 FAX 858-751-2435
    LPRng - Print Spooler (http://www.lprng.com)

2. You will need to:

   Use GNU Make or BSD Make.  Don't even think about trying to use
     another make unless you are a Wizard.  And even the Wizards would
     download the GNU Make.

   Use an ANSI C compiler.  GCC is preferred.

   READ the README file.  All the way through... Come on now,
   it's not THAT long.

   THEN do the install.

3. Utilities

   If you want to do text to postscript or whatever conversions,
   you will need a converter and program to determine input file type.

    file - utility to determine file type
       ftp://ftp.astron.com/pub/file/
       ftp://ftp.deshaw.com/pub/file/
     OR
	   ftp://ftp.lprng.com/pub/LPRng/UNIXTOOLS/file/

    A2PS - Ascii Text to PostScript Converter
       http://www-inf.enst.fr/~demaille/a2ps
       ftp://ftp.enst.fr/pub/unix/a2ps/

    GhostScript - PostScript converter
        http://www.cs.wisc.edu/~ghost/index.html
        http://www.ghost.com/

4. Read the Warnings Below For Your System

   Solaris Users:

      You can get precompiled versions of the GCC compiler
    and GMAKE from:
       www.sunfreeware.com
    Use the install in '/usr/local/' versions.
    This code has NOT been tested with the Sun Microsystems compilers,
    and NOT been tested with 64 bit support enabled.

      You have been warned.

      You MUST repeat MUST set your PATH so that the GNU gcc and
    other utilities in /usr/local/bin are found FIRST and then you
    MUST have the /usr/ccs/bin next in your path.

      PATH=/usr/local/bin:/usr/ccs/bin:$PATH

    If you have ANY problems with compilation,  and you are NOT
    using a 'clean' GCC installation,  please install GCC from
    a package (see http://www.sunfreeware.com) or recompile gcc
    and reinstall it.  Also,  please make sure that the include
    files are correct for your version of Solaris.  In fact,
    I recommend that you do the compilation on a 'clean' machine
    that has nothing but a 'virgin' Solaris Install + utilities
    BEFORE reporting problems.

  HPUX Users:
    See the following site for precompiled GCC and other tools:
         http://hpux.cae.wisc.edu/

    I STRONGLY recommend installing GCC and using GCC,  especially
    on HPUX 9.X and 10.X.  Make sure that your include files are
    the correct ones for your particular OS.

5. Installation and File Locations 

   The configure  utility is used to set the various file locations:

    ${prefix}       -   default /usr/local
    ${exec_prefix}  -   default ${prefix}  (/usr/local)
    ${libexecdir}   -   default ${exec_prefix}/libexec  (/usr/local/libexec)
                        ifhp filter executables installed here by default
                        over-ride --with-filterdir
    ${sysconfdir}   -   default ${prefix}/etc   (/usr/local/etc)
                        ifhp.conf, ifhp.conf.sample installed here
                        override location and name --with-ifhp_conf

    The following options to configure allow you to override or specify
    these value:
      --prefix=PREFIX         install architecture-independent files in PREFIX
      --exec-prefix=EPREFIX   install architecture-dependent files in EPREFIX
      --libexecdir=DIR        program executables in DIR [EPREFIX/libexec]
      --sysconfdir=DIR        read-only single-machine data in DIR [PREFIX/etc]
      --with-ifhp_conf=path   ifhp.conf explicit locations, override defaults
      --with-filterdir=DIR    override for filter directory ${libexecdir}/filters)

   Files:

     ${sysconfdir}/ifhp.conf   - configuration file
     ${filterdir}/ifhp         - filter
         (${libexecdir}/filters/ifhp)

   To do the install in /usr/local/libexec/filters, /usr/local/etc use:
        configure
   To do the install in /usr/libexec/filters, /usr/local/etc use:
        configure --prefix=/usr --sysconfdir=/etc

7.  Testing and Configuration

   Read the ifhp.conf file,  and see if your printer is listed.
   If it is,  then you can try printing to it DIRECTLY and see
   if it works.  The ifhp/UTILS/one.ps file is a handy PostScript
   test file.  In the following, NAME is the configuration entry
   for the printer.

   
   a)  cp ifhp/UTILS/one.ps /tmp
       NAME=ifhp.conf configuration entry for printer

   b)  print to a file FIRST:

       ifhp -Tmodel=${NAME},dev=/tmp/out,trace </tmp/one.ps

       Check the file for correct output format.
       If you have problems or want to see what is happening in
       detail,  try:

       ifhp -Tmodel=${NAME},dev=/tmp/out,trace,debug=1 </tmp/one.ps

       More information?

       ifhp -Tmodel=${NAME},dev=/tmp/out,trace,debug=2 </tmp/one.ps

       Pathologically curious?

       ifhp -Tmodel=${NAME},dev=/tmp/out,trace,debug=10 </tmp/one.ps

   b)  Network Connection (Jetdirect Box, etc):
       IP=xx.xx.xx.xx  (IP address of host)
       PORT=9100
       ifhp -Tmodel=${NAME},dev=${HOST}%${PORT},trace </tmp/one.ps

       Parallel Port:
       PORT=/dev/lp0
       ifhp -Tmodel=${NAME},dev=${PORT},trace </tmp/one.ps

       If this works,  you are on the air.

   c)  Set up LPRng printcap entry and try:

       lp:
         :ifhp=model=NAME
         :filter=/.../ifhp

        Don't forget to do:
          checkpc -f
          lpc reread (or kill -1 LPD_PID)

        Then try:
          lpq -lll -Plp
          lpr -Plp /tmp/one.ps
          lpq -lll -Plp

6.  IFHP-HOWTO

    This is now the repository for all information on the IFHP filter.
    It is in the docs/IFHP-HOWTO directory,  and in several formats.
    Please read at least the part on printer configurations.

    There is even a simple tutorial section, explaining how
    to debug your printer setup.

7.  Run time options are provided by the /usr/local/etc/ifhp.conf file.
    This file is extremely complex,  as it has the ability to configure
    just about any option that you can think of.  See the IFHP-HOWTO
    file in the docs directory for details.

    Do NOT change any of the contents of this file unless you are aware
    of the details.  You can OVERRIDE any of the values by adding sections
    at the end.  You can override the default values by adding a
    [ default ]  section,  and add your own entries.  See comments in the
    ifhp.conf file, and please read the HOWTO document.

8. Printcap Entry (LPRng)

    lp:
        # options for ifhp
        :ifhp= <ifhp options>
        # Default filter
        :filter=/usr/local/libexec/filters/ifhp:

     Example:
    lp:
        :ifhp=model=postscript,a4
        :filter=/usr/local/libexec/filters/ifhp:

    By default, the ifhp filter will check the job files for printability
    and/or postscript format.  If it is PostScript,  it will set the
    printer to PostScript mode using the PJL commands,  otherwise it
    will set the printer to PCL.  If you do not want this to happen,
    then print the job as a binary file;  this normall suppresses this
    checks.

    If you are doing banner page printing then add the following:

    lp:
        # Default filter and options
        :ifhp=model=postscript,a4
        :filter=/usr/local/libexec/filters/ifhp:
        # banner page support - disable 'suppress header page'
        :sh@
        :of=/usr/local/libexec/filters/ifhp:
        # PostScript banner
        :bp=/usr/local/libexec/filters/psbannner
        # PCL banner
        #:bp=/usr/local/libexec/filters/pclbannner

9. ifhp accepts the full range of LPD filter options and uses or
    or ignores them as applicable. The following command line options
    are used:
    -c  cleartext - dump file this directly to the printer, no fonts,
        no nothing done.  For those desperate people trying to
        find a way to print those obnoxious files.
        This is enabled by lpr -b  or lpr -l

    -sstatusfile    - the name of a status file that is updated with
        'rolling status' - i.e. - the last few log messages.
        The statusfile value is set from the 'ps=statusfile' printcap
        option

    -nuser, -hhost, -Jjob  - used by ifhp for accounting
    -Z[filteroptions,]* - filter options
        See the ifhp man page for filteroption details.

   Note: -T is not supplied by LPRng, and can be used to pass command line
      options to ifhp.
    -T[filteroptions,]* - filter options

$Id: README,v 1.18 2003/09/04 18:38:29 papowell Exp papowell $
