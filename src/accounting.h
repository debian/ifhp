/**************************************************************************
 * LPRng IFHP Filter
 * Copyright 1994-1999 Patrick Powell, San Diego, CA <papowell@astart.com>
 **************************************************************************/
/**** HEADER *****
$Id: accounting.h,v 1.1 1999/12/17 02:04:56 papowell Exp papowell $
 **** HEADER *****/

#if !defined(_ACCOUTING_H_)
#define _ACCOUTING_H_ 1
/* PROTOTYPES */
void Do_accounting(int start, int elapsed, int pagecounter, int npages );

#endif
