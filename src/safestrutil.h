/***************************************************************************
 * LPRng - An Extended Print Spooler System
 *
 * Copyright 1988-2001, Patrick Powell, San Diego, CA
 *     papowell@lprng.com
 * See LICENSE for conditions of use.
 * $Id: safestrutil.h,v 1.1 2002/01/30 14:34:20 papowell Exp papowell $
 ***************************************************************************/



#ifndef _SAFESTRUTIL_H_
#define _SAFESTRUTIL_H_ 1

#define safestrncat( s1, s2 ) mystrncat(s1,s2,sizeof(s1))
#define safestrncpy( s1, s2 ) mystrncpy(s1,s2,sizeof(s1))
#ifndef ISNULL
#define ISNULL(X) ((X)==0||(*X)==0)
#endif
#ifndef cval
#define cval(x) (int)(*(unsigned const char *)(x))
#endif



/* PROTOTYPES */
int safestrlen( const char *s1 );
int safestrcasecmp (const char *s1, const char *s2);
int safestrncasecmp (const char *s1, const char *s2, int len );
int safestrcmp( const char *s1, const char *s2 );
int safestrncmp( const char *s1, const char *s2, int len );
char *safestrchr( const char *s1, int c );
char *safestrrchr( const char *s1, int c );
char *safestrpbrk( const char *s1, const char *s2 );
char *safestrstr( const char *s1, const char *s2 );
void lowercase( char *s );
void uppercase( char *s );
char *trunc_str( char *s);
int Lastchar( char *s );
const char *Is_clean_name( const char *s );
void Clean_name( char *s );
int Is_meta( int c, const char * safe_chars );
const char *Find_meta( const char *s, const char *safe_chars );
void Clean_meta( char *t, const char *safe_chars );
char *Make_pathname( const char *dir,  const char *filename, MEMPASS );
int safeunlink( const char *s );
char *mystrncat( char *s1, const char *s2, int len );
char *mystrncpy( char *s1, const char *s2, int len );

#endif
