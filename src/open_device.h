/**************************************************************************
 * LPRng IFHP Filter
 * Copyright 1994-1999 Patrick Powell, San Diego, CA <papowell@astart.com>
 **************************************************************************/
/**** HEADER *****
$Id: open_device.h,v 1.4 2001/08/26 20:12:01 papowell Exp papowell $
 **** HEADER *****/

#if !defined(_OPEN_DEVICE_H_)
#define  _OPEN_DEVICE_H_  1
/* PROTOTYPES */
void Open_device( char *device );
void Set_keepalive( int sock );
int Link_open( char * device );
const char *inet_ntop_sockaddr( struct sockaddr *addr,
    char *str, int len );

#endif
