#**************************************************************************
#* LPRng IFHP Filter
#* Copyright 1994-1999 Patrick Powell, San Diego, CA <papowell@astart.com>
#*
#* $Id: Makefile.in,v 1.42 2004/09/24 20:18:14 papowell Exp papowell $
#

SRC=@srcdir@
SHELL=@SHELL@
@SET_MAKE@
prefix=@prefix@
exec_prefix=@exec_prefix@
sysconfdir=@sysconfdir@
libexecdir=@libexecdir@
bindir=@bindir@
libdir=@libdir@
top_builddir=..

INSTALLCMD=@INSTALL@
FILTER_DIR=@FILTER_DIR@
IFHP_CONF=@IFHP_CONF@
CC      = @CC@
LDCC      = @LDCC@
LDFLAGS= @LDFLAGS@
LOCALEDIR= @LOCALEDIR@
PERL= @PERL@
# add additional definitions here

# libraries you will need (found by config), add more to end
LIBS= @LIBS@ @LTLIBINTL@


######### - no changes by configure after here #############


#=============================================================================
# List the directories you want to generate:
# DIRS for all, clean, etc.
# ALLDIRS for other such as documentation
#=============================================================================

# TEST_FLAGS= -DFORMAT_TEST -Wall -Wformat -Werror

CFLAGS:= @CPPFLAGS@ @CFLAGS@ @DEFS@ \
	-I. -I.. -I${SRC} $(TEST_FLAGS) \
	-DIFHP_CONF=\"$(IFHP_CONF)\" -DLOCALEDIR=\"$(LOCALEDIR)\"

LINK =  $(LDCC) $(LDFLAGS) -o $@
STRIP=@STRIP_OPTION@

VPATH=.:..:${SRC}

# commands
INSTALL= wrapper snmp_printer_status extract_pjl
UTILITIES=snmp_printer_status extract_pjl
DATAFILES= snmp_printer_status.conf
COMMANDS= ifhp textps
EXTRA=monitor

all: ${COMMANDS} ${INSTALL} ${EXTRA}

snmp_printer_status: ${SRC}/snmp_printer_status.template Makefile
	sed -e 's,.PERL@,@PERL@,g' -e 's,.FILTER_DIR@,${FILTER_DIR},g' \
		${SRC}/snmp_printer_status.template \
	>snmp_printer_status
	chmod 755 snmp_printer_status

.PHONY: all clean install

#
# Object - Source  dependencies
#

########################################################################

OBJS= \
	ifhp.o accounting.o checkcode.o debug.o errormsg.o \
	globmatch.o open_device.o \
	perlobj.o plp_snprintf.o safemalloc.o \
	safestrutil.o stty.o vars.o


ifhp:	$(OBJS)
	${LINK} $(OBJS) ${LIBS}

$(OBJS): Makefile

monitor.o $(OBJS): debug.h errormsg.h ifhp.h portable.h \
	patchlevel.h config.h perlobj.h

monitor: monitor.o plp_snprintf.o safemalloc.o safestrutil.o errormsg.o
	${LINK} monitor.o plp_snprintf.o safemalloc.o safestrutil.o errormsg.o ${LIBS}

textps: textps.o plp_snprintf.o
	${LINK} textps.o plp_snprintf.o ${LIBS}

#
# Installation of commands
#

install: all
	if [ ! -d ${DESTDIR}$(FILTER_DIR) ] ; then ${SHELL} ${SRC}/mkinstalldirs ${DESTDIR}$(FILTER_DIR); fi;
	if [ ! -d ${DESTDIR}$(bindir) ] ; then ${SHELL} ${SRC}/mkinstalldirs ${DESTDIR}$(bindir); fi;
	if [ -n "${STRIP}" ] ; then strip $(COMMANDS); fi;
	if [ -n "${VERBOSE_INSTALL}" ] ; then set -x; fi; \
	set -e; \
	for i in $(COMMANDS) $(INSTALL) $(UTILITIES) ; do \
		$(INSTALLCMD) -m 755 $$i ${DESTDIR}$(FILTER_DIR)/`basename $$i`; \
	done ; \
	for i in $(UTILITIES) ; do \
		$(INSTALLCMD) -m 755 $$i ${DESTDIR}$(bindir)/`basename $$i`; \
	done ; \
	for i in $(DATAFILES) ; do \
		$(INSTALLCMD) -m 644 ${SRC}/$$i ${DESTDIR}$(FILTER_DIR)/`basename $$i`; \
	done ;
#
# Miscellaneous
#

clean:
	-rm -f $(COMMANDS) ${EXTRA} *.o *.core core ? tags ofhp

CI=
#CO=-kv
CO=-l
ci: 
	if test ! -d RCS ; then mkdir RCS; fi;
	checkin() { \
		rcs -l $$1; \
		ci $(CI) -mUpdate -t-Initial $$1; \
		co $(CO) $$1; \
	}; \
	for i in *.[ch] Makefile* *.sh_init *.ps *.conf; \
		do \
		if [ -f $$i ]; then checkin $$i; fi; \
	done;

distclean: clean
	-rm -f Makefile *.bak *.orig *.rej wrapper

tags TAGS:
	ctags *.c *.h
