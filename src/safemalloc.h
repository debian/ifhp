/***************************************************************************
 * LPRng - An Extended Print Spooler System
 *
 * Copyright 1988-2001, Patrick Powell, San Diego, CA
 *     papowell@lprng.com
 * See LICENSE for conditions of use.
 * $Id: safemalloc.h,v 1.2 2002/03/06 04:43:36 papowell Exp papowell $
 ***************************************************************************/



#ifndef _SAFEMALLOC_H_
#define _SAFEMALLOC_H_ 1

#define MEMPASS const char *file, int line
#define MEMPASSED file, line
#define MEMINFO __FILE__, __LINE__
#define SAFEFREE(x) safefree(x, __FILE__, __LINE__)
#ifndef SMALLBUFFER
# define SMALLBUFFER 512
#endif
#ifndef LARGEBUFFER
# define LARGEBUFFER 10240
#endif

/* PROTOTYPES */
void *malloc_or_die( size_t size, const char *file, int line );
void *realloc_or_die( void *p, size_t size, const char *file, int line );
void safefree( void *p, const char *file, int line );
char *safestrdup (const char *p, const char *file, int line);
char *safestrdup2( const char *s1, const char *s2, const char *file, int line );
char *safeextend2( char *s1, const char *s2, const char *file, int line );
char *safestrdup3( const char *s1, const char *s2, const char *s3,
	const char *file, int line );
char *safeextend3( char *s1, const char *s2, const char *s3,
	const char *file, int line );
char *safeextend4( char *s1, const char *s2, const char *s3, const char *s4,
	const char *file, int line );
char *safestrdup4( const char *s1, const char *s2,
	const char *s3, const char *s4,
	const char *file, int line );
char *safeextend5( char *s1, const char *s2, const char *s3, const char *s4, const char *s5,
	const char *file, int line );
char *safestrdup5( const char *s1, const char *s2,
	const char *s3, const char *s4, const char *s5,
	const char *file, int line );

#endif
