/**************************************************************************
 * LPRng IFHP Filter
 * Copyright 1994-1999 Patrick Powell, San Diego, CA <papowell@astart.com>
 **************************************************************************/
/**** HEADER *****
$Id: stty.h,v 1.1 1999/12/17 02:04:59 papowell Exp papowell $
 **** HEADER *****/

#if !defined(_STTY_H_)
#define _STTY_H_ 1
/* PROTOTYPES */
void Do_stty( int fd, char *Stty_command );

#endif
