/**************************************************************************
 * LPRng IFHP Filter
 * Copyright 1994-1999 Patrick Powell, San Diego, CA <papowell@astart.com>
 **************************************************************************/
/**** HEADER *****
$Id: errormsg.h,v 1.6 2002/03/06 04:43:33 papowell Exp papowell $
 **** ENDHEADER ****/

#ifndef _ERRORMSG_H
#define _ERRORMSG_H

#if defined(FORMAT_TEST)
#define LOGMSG(X) printf(
#define FATAL(X) printf(
#define LOGERR(X) printf(
#define LOGERR_DIE(X) printf(
#define LOGDEBUG printf
#define DIEMSG printf
#define WARNMSG printf
#define MESSAGE printf
#else
#define LOGMSG(X) logmsg(
#define FATAL(X) fatal(
#define LOGERR(X) logerr(
#define LOGERR_DIE(X) logerr_die(
#define LOGDEBUG logDebug
#define DIEMSG Diemsg
#define WARNMSG Warnmsg
#define MESSAGE Message
#endif

/* PROTOTYPES */
const char * Errormsg ( int err );
/* VARARGS2 */
#ifdef HAVE_STDARGS
 void logmsg( char *msg,...)
#else
 void logmsg(va_alist) va_dcl
#endif
;
/* VARARGS2 */
#ifdef HAVE_STDARGS
 void logDebug( char *msg,...)
#else
 void logDebug(va_alist) va_dcl
#endif
;
/* VARARGS2 */
#ifdef HAVE_STDARGS
 void fatal ( char *msg,...)
#else
 void fatal (va_alist) va_dcl
#endif
;
/* VARARGS2 */
#ifdef HAVE_STDARGS
 void logerr (char *msg,...)
#else
 void logerr (va_alist) va_dcl
#endif
;
/* VARARGS2 */
#ifdef HAVE_STDARGS
 void logerr_die ( char *msg,...)
#else
 void logerr_die (va_alist) va_dcl
#endif
;
char *Time_str(int shortform, time_t t);
int Write_fd_str( int fd, const char *str );
int Write_fd_len( int fd, const char *str, int len );
const char *Sigstr (int n);
void setstatus( char *msg, char *details, int trace );
int udp_open( char *device );
/* VARARGS2 */
#ifdef HAVE_STDARGS
 void safefprintf (int fd, char *format,...)
#else
 void safefprintf (va_alist) va_dcl
#endif
;

#endif
