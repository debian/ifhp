/**************************************************************************
 * LPRng IFHP Filter
 * Copyright 1994-1999 Patrick Powell, San Diego, CA <papowell@astart.com>
 **************************************************************************/
/**** HEADER *****/
 static char *const _id = "$Id: accounting.c,v 1.18 2004/02/25 15:56:24 papowell Exp papowell $";

#include "ifhp.h"
#include "open_device.h"

/**** ENDINCLUDE ****/

/*
 *  Do_accounting()
 *  writes the accounting information to the accounting file
 *  READ THE IFHP HOWTO FOR DETAILS!
 *
 *  Set up a queue and experiment with output formats.
 *
 *  This stuff is provided as a courtesy.  If you want to use
 *  accounting read the IFHP HOWTO, the LPRng HOWTO,  and various
 *  warnings about abuse of printing and accounting.
 *
 *  The notion here is that the ifhp script will put a line
 *  into the accounting file at the start and end of the job.
 *  This code is ugly and I don't have time to beautify it.
 *  Contact me for LPRng support contract prices.
 */


void Do_accounting(int start, int elapsed, int pagecounter, int npages )
{
	int accounting_fd = -1, c, n;
	char working[SMALLBUFFER];
	char *list, *s, *t;

	if( start ){
	LOGMSG(LOGINFO) _("Do_accounting: pagecounter %d"), pagecounter);
	} else {
	LOGMSG(LOGINFO) _("Do_accounting: pagecounter %d, pages %d"), pagecounter, npages);
	}
	DEBUG3("Accounting script '%s', file '%s', npages %d",
		Accounting_script, Accountfile, npages );

	list = 0;
	/* we first set up the output line and arguments */
	if( start ){
		list = safeextend2(list, OF_Mode?"start":"filestart",__FILE__,__LINE__);
	} else {
		list = safeextend2(list,  OF_Mode?"end":"fileend",__FILE__,__LINE__);
		SNPRINTF( working, sizeof(working))  " '-b%d'", npages );
		list = safeextend2(list,  working,__FILE__,__LINE__);
		SNPRINTF( working, sizeof(working))  " '-T%d'", elapsed );
		list = safeextend2(list,  working,__FILE__,__LINE__);
	}
	SNPRINTF( working, sizeof(working))  " '-q%d'", getpid() );
	list = safeextend2(list,  working,__FILE__,__LINE__);
	SNPRINTF( working, sizeof(working))  " '-p%d'", pagecounter );
	list = safeextend2(list,  working,__FILE__,__LINE__);
	SNPRINTF( working, sizeof(working))  " '-t%s'", Time_str(0,0) );
	list = safeextend2(list,  working,__FILE__,__LINE__);
	DEBUG1("Accounting: script '%s', Accountfile '%s', output '%s'",
		Accounting_script, Accountfile, list );

	

	/* this is probably the best way to do the output */
	if( !ISNULL(Accountfile) ){
		if( strchr(Accountfile,'%') ){
			accounting_fd = Link_open( Accountfile);
		} else {
			accounting_fd = open(Accountfile, O_WRONLY|O_APPEND );
		}
	}
	if( accounting_fd > 0 ){
		/*Set_max_fd(accounting_fd);*/
		for( s = Accounting_info; *s; ++s ){
			c = cval(s);
			t = 0;
			if( isupper(c) ) t = Upperopts[c-'A'];
			if( islower(c) ) t = Loweropts[c-'a'];
			if( t && *t ){
				SNPRINTF( working, sizeof(working))  " '-%c%s'", c, t );
				list = safeextend2(list,  working,__FILE__,__LINE__);
			}
		}
		DEBUG1("Accounting: writing to %d, '%s'", accounting_fd, list );
		n = strlen(list);
		list = safeextend2(list,  "\n",__FILE__,__LINE__);
	    Write_fd_str(accounting_fd,list);
		close( accounting_fd); accounting_fd = -1;
		list[n] = 0;
	}

	if( Accounting_script && *Accounting_script ){
		s = safestrdup3( Accounting_script, " ", list, __FILE__,__LINE__ );
		for( c = 'A'; c <= 'Z'; ++c ){
			if( strchr( "T", c ) ) continue;
			t = Upperopts[c-'A'];
			if( t && *t ){
				SNPRINTF( working, sizeof(working))  " '-%c%s'", c, t );
				s = safeextend2(s,  working,__FILE__,__LINE__);
			}
		}
		for( c = 'a'; c <= 'z'; ++c ){
			if( strchr( "bqpt", c ) ) continue;
			t = Loweropts[c-'a'];
			if( t && *t ){
				SNPRINTF( working, sizeof(working))  " '-%c%s'", c, t );
				s = safeextend2(s,  working,__FILE__,__LINE__);
			}
		}
		n = Filter_file( s, _("ACCOUNTING"), -1 , -1, 0, 0 );
		DEBUG1("Accounting: exec '%s'", s );
		if( n ){
			Errorcode = n;
			FATAL(LOG_INFO)_("Do_accounting: script '%s' failed"), Accounting_script);
		}
		if( s ) free(s); s = 0;
	}
	if( list ) free(list); list = 0;
}
