/**************************************************************************
 * LPRng IFHP Filter
 * Copyright 1994-1999 Patrick Powell, San Diego, CA <papowell@astart.com>
 **************************************************************************/
/**** HEADER *****/
static char *const _id = "$Id: vars.c,v 1.31 2003/11/17 16:55:37 papowell Exp papowell $";

#define EXTERN
#define DEF
#define DEFINE(X) X
#include "ifhp.h"

/**** ENDINCLUDE ****/

struct keyvalue Valuelist[] = {

    {  "Accounting_info", "accounting_info", &Accounting_info, STRV ,0 },
    {  "Accounting_script", "accounting", &Accounting_script, STRV ,0 },
    {  "Appsocket", "appsocket", (void *)&Appsocket, FLGV ,0 },
    {  "Autodetect", "autodetect", (void *)&Autodetect, FLGV,0 },
    {  "Close_appsocket", "close_appsocket", (void *)&Close_appsocket, FLGV,0 },
    {  "Crlf", "crlf", (void *)&Crlf, FLGV,0 },
    {  "Dev_retries", "dev_retries", (void *)&Dev_retries, INTV,0 },
    {  "Dev_sleep", "dev_sleep", (void *)&Dev_sleep, INTV,0 },
    {  "Device", "dev", &Device, STRV,0 },
    {  "End_status", "end_status", &End_status, STRV,0 },
    {  "Foomatic", "foomatic", (void *)&Foomatic, FLGV,0 },
    {  "Foomatic_rip", "foomatic_rip", &Foomatic_rip, STRV,0 },
    {  "Force_conversion", "forceconversion", (void *)&Force_conversion, FLGV,0 },
    {  "Force_processing", "forceprocessing", (void *)&Force_processing, FLGV,0 }, 
    {  "Force_status", "forcestatus", (void *)&Force_status, FLGV,0 },
    {  "Full_time", "fulltime", (void *)&Full_time, FLGV,0 },
    {  "Hpgl2", "hpgl2", (void *)&Hpgl2, FLGV,0 },
    {  "Ignore_eof", "ignore_eof", (void *)&Ignore_eof, FLGV,0 },
    {  "Initial_timeout", "initial_timeout", (void *)&Initial_timeout, INTV,0 },
    {  "Logall", "logall", (void *)&Logall, FLGV,0 },
    {  "Max_status_size", "statusfile_max", (void *)&Max_status_size, INTV,0 },
    {  "Min_status_size", "statusfile_min", (void *)&Min_status_size, INTV,0 },
    {  "No_udp_monitor", "no_udp_monitor", (void *)&No_udp_monitor, FLGV,0 },
    {  "Null_pad_count", "nullpad", (void *)&Null_pad_count, INTV,0 },
    {  "Pagecount", "pagecount", &Pagecount, STRV,0 },
    {  "Pagecount_end", "pagecount_end", (void *)&Pagecount_end, FLGV,0 },
    {  "Pagecount_interval", "pagecount_interval", (void *)&Pagecount_interval, INTV,0 },
    {  "Pagecount_poll", "pagecount_poll", (void *)&Pagecount_poll, INTV,0 },
    {  "Pagecount_poll_send", "pagecount_poll_end", (void *)&Pagecount_poll_end, INTV,0 },
    {  "Pagecount_poll_start", "pagecount_poll_start", (void *)&Pagecount_poll_start, INTV,0 },
    {  "Pagecount_start", "pagecount_start", (void *)&Pagecount_start, FLGV,0 },
    {  "Pagecount_timeout", "pagecount_timeout", (void *)&Pagecount_timeout, INTV,0 },
    {  "Pcl", "pcl", (void *)&Pcl, FLGV,0 },
    {  "Pcl_eoj_at_end", "pcl_eoj_at_end", (void *)&Pcl_eoj_at_end, FLGV,0 },
    {  "Pcl_eoj_at_start", "pcl_eoj_at_start", (void *)&Pcl_eoj_at_start, FLGV,0 },
    {  "Pjl", "pjl", (void *)&Pjl, FLGV,0 },
    {  "Pjl_console", "console", (void *)&Pjl_console, FLGV,0 },
    {  "Pjl_console", "pjl_console", (void *)&Pjl_console, FLGV,0 },
    {  "Pjl_display_size", "display_size", (void *)&Pjl_display_size, INTV,0 },
    {  "Pjl_display_size", "pjl_display_size", (void *)&Pjl_display_size, INTV,0 },
    {  "Pjl_done_msg", "pjl_done_msg", &Pjl_done_msg, STRV ,0 },
    {  "Pjl_enter", "pjl_enter", (void *)&Pjl_enter, FLGV,0 },
    {  "Pjl_ready_msg", "pjl_ready_msg", &Pjl_ready_msg, STRV ,0 },
    {  "Pjl_waitend_byjobname", "pjl_waitend_byjobname", (void *)&Pjl_waitend_byjobname, FLGV, 0},
    {  "Ppd_file", "ppd", &Ppd_file, STRV, 0},
    {  "Ps", "ps", (void *)&Ps, FLGV,0 },
    {  "Ps_ctrl_t", "ps_ctrl_t", (void *)&Ps_ctrl_t, FLGV,0 },
    {  "Ps_eoj", "ps_eoj", (void *)&Ps_eoj, FLGV,0 },
    {  "Ps_eoj_at_end", "ps_eoj_at_start", (void *)&Ps_eoj_at_end, FLGV,0 },
    {  "Ps_eoj_at_start", "ps_eoj_at_start", (void *)&Ps_eoj_at_start, FLGV,0 },
    {  "Ps_pagecount_code", "ps_pagecount_code", &Ps_pagecount_code, STRV,0 },
    {  "Ps_status_code", "ps_status_code", &Ps_status_code, STRV,0 },
    {  "Psonly", "psonly", (void *)&Psonly, FLGV,0 },
    {  "Qms", "qms", (void *)&Qms, FLGV ,0 },
    {  "Remove_ctrl", "remove_ctrl", &Remove_ctrl, STRV ,0 },
    {  "Remove_pjl_at_start", "remove_pjl_at_start", (void *)&Remove_pjl_at_start, FLGV ,0 },
    {  "Send_job_rw_timeout", "send_job_rw_timeout", (void *)&Send_job_rw_timeout, INTV,0 },
    {  "Snmp_dev", "snmp_dev", &Snmp_dev, STRV,0 },
    {  "Snmp_end_status", "snmp_end_status", &Snmp_end_status, STRV,0 },
    {  "Snmp_monitor", "snmp_monitor", (void *)&Snmp_monitor, FLGV,0 },
    {  "Snmp_program", "snmp_program", &Snmp_program, STRV,0 },
    {  "Snmp_status_fields", "snmp_status_fields", &Snmp_status_fields, STRV,0 },
    {  "Snmp_sync_status", "snmp_sync_status", &Snmp_sync_status, STRV,0 },
    {  "Snmp_wait_after_close", "snmp_wait_after_close", (void *)&Snmp_wait_after_close, INTV,0 },
    {  "Status", "status", (void *)&Status, FLGV,0 },
    {  "Status_ignore", "status_ignore", &Status_ignore, STRV,0 },
    {  "Status_translate", "status_translate", &Status_translate, STRV,0 },
    {  "Stty_args", "stty", &Stty_args, STRV,0 },
    {  "Sync", "sync", &Sync, STRV,0 },
    {  "Sync_interval", "sync_interval", (void *)&Sync_interval, INTV,0 },
    {  "Snmp_sync_status", "snmp_sync_status", &Snmp_sync_status, STRV,0 },
    {  "Sync_timeout", "sync_timeout", (void *)&Sync_timeout, INTV,0 },
    {  "Tbcp", "tbcp", (void *)&Tbcp, FLGV,0 },
    {  "Text", "text", (void *)&Text, FLGV,0 },
    {  "Trace_on_stderr", "trace", (void *)&Trace_on_stderr, FLGV,0 },
    {  "Wait_for_banner_page", "wait_for_banner", (void *)&Wait_for_banner, FLGV,0 },
    {  "Waitend", "waitend", &Waitend, STRV,0 },
    {  "Waitend_ctrl_t_interval", "waitend_ctrl_t_interval", (void *)&Waitend_ctrl_t_interval, INTV,0 },
    {  "Waitend_interval", "waitend_interval", (void *)&Waitend_interval, INTV,0 },
    {  "Waitend_timeout", "waitend_timeout", (void *)&Waitend_timeout, INTV,0 },

    { 0, 0, 0, 0, 0 }
};
