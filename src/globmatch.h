/**************************************************************************
 * LPRng IFHP Filter
 * Copyright 1994-1999 Patrick Powell, San Diego, CA <papowell@astart.com>
 **************************************************************************/
/**** HEADER *****
$Id: globmatch.h,v 1.1 1999/12/17 02:04:57 papowell Exp papowell $
 **** HEADER *****/

#if !defined(_GLOBMATCH_H_)
#define _GLOBMATCH_H_ 1
/* PROTOTYPES */
int Globmatch( char *pattern, char *str );

#endif
